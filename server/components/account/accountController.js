const express = require('express');

const router = express.Router();

const bodyParser = require('body-parser');

const argon2 = require('argon2');

const User = require('../../models/userModel');

const isAuth = require('../../middleware/isAuth');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

/**
 * res means the response data container (usually in json)
 * When user sign up or sign in, return access token (JWT) in res.data.token
 */

// SIGN UP
router.post('/accounts/signup', async (req, res) => {
  const {
    email, firstName, lastName,
  } = req.body;
  const password = await argon2.hash(req.body.password);
  const user = new User({
    email, password, firstName, lastName,
  });
  user.save().then(() => user.generateRefreshToken()).then((tokens) => {
    const { refreshToken, accessToken } = tokens;
    const { _id } = user;
    res.status(201).send({
      status: 'ok',
      message: 'User signed up successfully.',
      data: {
        email, _id, firstName, lastName, refreshToken, accessToken,
      },
    });
  }).catch((e) => {
    let message = 'An error has occurred!';
    let statusCode = '';
    if (e.code === 11000) { message = 'The email has already been used.'; statusCode = 'duplicate_email'; }
    res.status(400).send({ status: 'error', message, statusCode });
  });
});

// SIGN IN
router.post('/accounts/signin', (req, res) => {
  const { email, password } = req.body;
  // eslint-disable-next-line arrow-body-style
  User.findByCredentials(email, password).then((user) => {
    return user.generateRefreshToken().then((refreshToken, accessToken) => {
      const { _id, firstName, lastName } = user;
      res.send({
        status: 'ok',
        message: 'User signed in successfully.',
        data: {
          email, _id, firstName, lastName, refreshToken, accessToken,
        },
      });
    });
  }).catch(() => {
    // TODO: may want to handle returned new Error
    res.status(400).send({ status: 'error', message: 'Your email or password is incorrect', statusCode: 'bad_credential' });
  });
});

//  SIGN OUT
router.post('/accounts/signout', isAuth, (req, res) => {
  req.user.removeRefreshToken(req.body.refreshToken).then(() => {
    res.status(200).send({ status: 'ok', message: 'User signed out successfully.', data: {} });
  }).catch(() => {
    // TODO: need catch Error
    res.status(400).send({ status: 'error', message: 'An error has occurred!' });
  });
});

module.exports = router;
