/**
 * Middleware for private routes
 * Check for jwt token from Authorization header (Bearer <JWT>)
 * jwt.verify check validity and extract sub (userId)
 * If matched, return the user mongodb document obj (be careful, including everything even password)
 */
const jwt = require('jsonwebtoken');

//  const User = require('../models/userModel');

const isAuth = (req, res, next) => {
  if (req.header('Authorization') === undefined) res.status(401).send({ status: 'error', message: 'Access Token is not found.', statusCode: 'no_token' });
  const token = req.header('Authorization').split(' ')[1];

  let decoded;
  try {
    decoded = jwt.verify(token, process.env.jwtKey);
  } catch (e) {
    //  https://github.com/auth0/node-jsonwebtoken#errors--codes
    let message = '';
    let statusCode = '';
    switch (e.name) {
      case 'TokenExpiredError': message = 'Access Token has expired.'; statusCode = 'expired_token'; break;
      case 'JsonWebTokenError': message = 'Access Token is not valid.'; statusCode = 'invalid_token'; break;
      default: message = 'An error has occurred.'; break;
    }
    message = `You are not authorized to access this resource: ${message}`;
    res.status(401).send({ status: 'error', message, statusCode });
  }

  // return document for _id attached in jwt
  this.findOne({
    _id: decoded.sub,
  }).then((user) => {
    if (!user) {
      return Promise.reject(new Error('BadToken'));
    }
    /**
     * Important: the returned req.user is now a full user document, which contains the password
     */
    req.user = user;
    next();
    return true;
  }).catch((e) => {
    let message = '';
    let statusCode = '';
    switch (e) {
      case 'Error: BadToken': message = 'Access Token is not linked to any user.'; statusCode = 'bad_token'; break;
      default: message = 'An error has occurred.'; break;
    }
    message = `You are not authorized to access this resource: ${message}`;
    res.status(401).send({ status: 'error', message, statusCode });
  });
};
module.exports = isAuth;
