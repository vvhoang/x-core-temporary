const mongoose = require('mongoose');

const validator = require('validator');

const jwt = require('jsonwebtoken');

const argon2 = require('argon2');

const crypto = require('crypto');

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    require: true,
    trim: true,
    minlength: 1,
    unique: true,
    index: true,
    validate: {
      validator: validator.isEmail,
      message: '{VALUE} is not a valid email',
    },
  },
  /** username: {
    type: String,
    unique: true,
    index: true,
  },
  */
  password: {
    type: String,
    require: true,
    minlength: 6,
  },
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  birthDate: {
    type: Date,
  },
  tokens: [{
    access: {
      type: String,
    },
    token: {
      type: String,
    },
  }],
});

/**
 * generateRefreshToken:
 * generate refreshToken -> save to db
 * generate accessToken
 * return refreshToken and accessToken
 * ----------------------
 * generateAccessToken(refreshToken):
 * lookup refreshToken in database -> existed? -> generate accessToken (JWT)
 */

UserSchema.methods.generateRefreshToken = function () {
  //  TODO: Replacing crypto for security reason
  const refreshToken = crypto.randomBytes(12).toString('hex');
  this.tokens.push({ access: 'refresh', token: refreshToken });
  const tokenPayload = {
    //  IMPORTNT: _id is being used, assumed to be the same with userId
    userId: this._id.toString(),
    email: this.email,
    firstName: this.firstName,
    lastName: this.lastName,
  };
  //  https://github.com/auth0/node-jsonwebtoken#jwtsignpayload-secretorprivatekey-options-callback
  const accessToken = jwt.sign(tokenPayload, process.env.jwtKey, {
    subject: this._id.toString(),
    //  audience: <appId> attached appId of the game or app
    //  issuer: something that represent this core endpoint / maybe the app endpoint
    expiresIn: process.env.jwtKeyExpiresIn,
  }).toString();
  return this.save().then(() => ({ refreshToken, accessToken }));
};

UserSchema.methods.removeRefreshToken = function (token) {
  return this.update({
    $pull: {
      tokens: { token },
    },
  });
  //  TODO: catch Rejected Promise
};

//  Credentials Authentication
UserSchema.statics.findByCredentials = function (email, password) {
  return this.findOne({ email }).then((user) => {
    if (!user) {
      //  No user with email found
      //  TODO: catch Rejected Promise
      return Promise.reject(new Error('badEmail'));
    }
    return argon2.verify(user.password, password).then((res) => {
      // Check pass
      if (res) return Promise.resolve(user);
      return Promise.reject(new Error('badPassword'));
    });
  });
};

module.exports = mongoose.model('User', UserSchema);
