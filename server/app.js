require('./config/config');

const express = require('express');

const app = express();

require('./db/mongoose');

const UserController = require('./components/account/accountController');

app.use('/api', UserController);

module.exports = app;
