# x-core
Core backend application
## Conventions
### Javascript
Use [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript).
### Naming
Database Collection, Index, Key: camelCase
Objects, functions, variables: camelCase
Class, Constructor (like MongoDB model): PascalCase
## Server API

### Standards
#### Response
```
{
    status: ok | error <required>,
    statusCode: <optional, can be error code>,
    message: <user-readable message, optional but recommeded>,
    data: <payload>
}
```
### Endpoints
#### Status code `statusCode`
Status code I made up to use in both server and client

|code|meaning|
|----|----|
|no_token|No token is found in the request|
|bad_token|Token does not existed in database|
|expired_token|Token has already expired (jwt.verify)|
|invalid_token|Token is not a valid JWT token|
|duplicate_email|Email has already existed in database|
|bad_credential|Incorrect email or password|

#### Accounts
IMPORTANT: This endpoint must not be visible to the frontend as it contains
##### POST /accounts/signup
`Request`
```
{
    email,
    password,
    firstName,
    lastName,
}
```
`Response`
```
{
    email,
    _id,
    firstName,
    lastName,
    refreshToken,
    accessToken,
}
```
##### POST /accounts/signin
`Request`
```
{
    email,
    password
}
```
`Response`
Same as `POST /users/signup`
##### POST /accounts/signout `Private`
`Request`
No `Body`, only `Authentication header` required.
